| 库名                | 链接                                                                       |
|---------------------|----------------------------------------------------------------------------|
| argparse            | -                                                                          |
| bustache            | -                                                                          |
| chaiscript          | -                                                                          |
| CImg                | [https://github.com/GreycLab/CImg/releases/tag/v.3.2.3](https://github.com/GreycLab/CImg/releases/tag/v.3.2.3) |
| cpp_httplib         | -                                                                          |
| emhash              | -                                                                          |
| expected            | -                                                                          |
| imgui               | -                                                                          |
| jwt-cpp             | [https://github.com/Thalhammer/jwt-cpp/](https://github.com/Thalhammer/jwt-cpp/) |
| libzippp            | -                                                                          |
| loguru              | -                                                                          |
| magic_enum          | -                                                                          |
| nlohmann json       | [https://github.com/nlohmann/json/releases/tag/v3.11.2](https://github.com/nlohmann/json/releases/tag/v3.11.2) |
| oatpp               | -                                                                          |
| oatpp-curl          | -                                                                          |
| oatpp-openssl       | -                                                                          |
| oatpp-sqlite        | -                                                                          |
| oatpp-swagger       | -                                                                          |
| oatpp-websocket     | -                                                                          |
| oatpp-zlib          | -                                                                          |
| pugixml             | [https://github.com/zeux/pugixml/releases/tag/v1.13](https://github.com/zeux/pugixml/releases/tag/v1.13) |
| toml++              | -                                                                          |
