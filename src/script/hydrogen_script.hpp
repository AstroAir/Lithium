#include "core/hydrogenccd.h"
#include "core/hydrogenccdchip.h"
#include "core/hydrogencontroller.h"
#include "core/hydrogencorrelator.h"
#include "core/hydrogendetector.h"
#include "core/hydrogendome.h"
#include "core/hydrogendriver.h"
#include "core/hydrogenfilterwheel.h"
#include "core/hydrogenfocuser.h"
#include "core/hydrogengps.h"
#include "core/hydrogenreceiver.h"
#include "core/hydrogenrotator.h"
#include "core/hydrogenspectrograph.h"
#include "core/hydrogentelescope.h"
#include "core/hydrogenweather.h"
