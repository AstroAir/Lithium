#ifndef UNDERSCORE_H
#define UNDERSCORE_H

#include <string>

bool HasUppercase(const std::string& str);
std::string ToUnderscore(const std::string& str);
std::string ToCamelCase(const std::string& str);
std::string ConvertToUnderscore(const std::string& str);
std::string ConvertToCamelCase(const std::string& str);

#endif  // UNDERSCORE_H
