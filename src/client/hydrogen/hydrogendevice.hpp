#pragma once

#include "modules/client/indiclient.hpp"
#include "core/camera.hpp"
#include "core/property/task/device_task.hpp"
#include "core/property/task/conditional_task.hpp"
#include "core/property/task/loop_task.hpp"

#include "core/base/basedevice.h"
#include "core/property/hydrogenproperty.h"

#include <string>
#include <atomic>