/* Version information */
#define LithiumWebManagerApp_VERSION "@LithiumWebManagerApp_VERSION_MAJOR@.@LithiumWebManagerApp_VERSION_MINOR@"

/* Build TS */
#cmakedefine INDI_WEB_MANAGER_APP_BUILD_TS "@INDI_WEB_MANAGER_APP_BUILD_TS@"
